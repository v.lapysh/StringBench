package com.presentation.benchmark.buffer;

public class StringBufferConcurrent {
    private static StringBuffer sbuf = new StringBuffer();
    private static StringBuilder sbuild = new StringBuilder();

    public void testBuilder() {
        Thread thread1 = new Thread() {
            public void run() {
                int cnt = 50;
                while (cnt-- > 0) {
                    System.out.println(sbuild.append("blo"));
                }
            }
        };
        Thread thread2 = new Thread() {
            public void run() {
                int cnt = 50;
                while (cnt-- > 0) {
                    System.out.println(sbuild.append("ha"));
                }
            }
        };
        thread1.start();
        thread2.start();
    }

    public void testBuffer() {
        Thread thread1 = new Thread() {
            public void run() {
                int cnt = 50;
                while (cnt-- > 0) {
                    System.out.println(sbuf.append("blo"));
                }
            }
        };
        Thread thread2 = new Thread() {
            public void run() {
                int cnt = 50;
                while (cnt-- > 0) {
                    System.out.println(sbuf.append("ha"));
                }
            }
        };
        thread1.start();
        thread2.start();
    }

    public static void main(String[] args) {
        //new StringBufferConcurrent().testBuffer();
        new StringBufferConcurrent().testBuilder();
    }
}
