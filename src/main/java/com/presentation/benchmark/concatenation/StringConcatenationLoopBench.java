package com.presentation.benchmark.concatenation;

import org.apache.commons.text.RandomStringGenerator;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
public class StringConcatenationLoopBench {

    @Param({"100", "500", "1000"})
    public int size;
    String[] strings;

    RandomStringGenerator randomStringGenerator = new RandomStringGenerator.Builder().withinRange('a', 'z').build();

    @Setup
    public void setup() {
        strings = new String[size];
        //Random random = new Random(1337);
        for (int c = 0; c < size; c++) {
            strings[c] = randomStringGenerator.generate(10_000);
        }
    }

    public String append() {
        StringBuilder builder = new StringBuilder();
        for (String s : strings) {
            builder.append(s);
        }
        return builder.toString();
    }

    public String bufferAppend() {
        StringBuffer buffer = new StringBuffer();
        for (String s : strings) {
            buffer.append(s);
        }
        return buffer.toString();
    }

    public String concat() {
        String str = "";
        for (String s : strings) {
            str = str.concat(s);
        }
        return str;
    }

    public String concatByPlus() {
        String str = "";
        for (String s : strings) {
            str = str + s;
        }
        return str;
    }

    @Benchmark
    public void measureAppend(Blackhole bh) {
        bh.consume(append());
    }

    @Benchmark
    public void measureBufAppend(Blackhole bh) {
        bh.consume(bufferAppend());
    }

    @Benchmark
    public void measureConcat(Blackhole bh) {
        bh.consume(concat());
    }

    @Benchmark
    public void measureConcatByPlus(Blackhole bh) {
        bh.consume(concatByPlus());
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(StringConcatenationLoopBench.class.getSimpleName())
                .warmupIterations(5)
                .measurementIterations(7)
                //.jvm("C:\\Program Files\\Java\\jre1.8.0_144\\bin\\javaw.exe")
                .jvmArgs("-Xms3g", "-Xmx3g")
                .shouldDoGC(true)
                .threads(4)
                .forks(1)
                .build();

        new Runner(opt).run();
    }
}
