package com.presentation.benchmark.concatenation;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.CompilerControl;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
@Warmup(iterations = 4, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class StringConcatenationNoJitBench {

    @State(Scope.Thread)
    public static class State2 {
        public String a = "abc";
        public String b = "xyz";
    }

    @State(Scope.Thread)
    public static class State3 {
        public String a = "abc";
        public String b = "xyz";
        public String c = "123";
    }

    @State(Scope.Thread)
    public static class State4 {
        public String a = "abc";
        public String b = "xyz";
        public String c = "123";
        public String d = "hdagmtghpslerkbxohrtbxckfgiucbmelqhwbgbahfbmfaazbbcu";
    }

    @Benchmark
    @CompilerControl(CompilerControl.Mode.EXCLUDE)
    public void plus_2(State2 state, Blackhole blackhole) {
        blackhole.consume(state.a+state.b);
    }

    @Benchmark
    @CompilerControl(CompilerControl.Mode.EXCLUDE)
    public void plus_3(State3 state, Blackhole blackhole) {
        blackhole.consume(state.a+state.b+state.c);
    }

    @Benchmark
    @CompilerControl(CompilerControl.Mode.EXCLUDE)
    public void plus_4(State4 state, Blackhole blackhole) {
        blackhole.consume(state.a+state.b+state.c+state.d);
    }

    @Benchmark
    @CompilerControl(CompilerControl.Mode.EXCLUDE)
    public void stringbuilder_2(State2 state, Blackhole blackhole) {
        blackhole.consume(new StringBuilder().append(state.a).append(state.b).toString());
    }

    @Benchmark
    @CompilerControl(CompilerControl.Mode.EXCLUDE)
    public void stringbuilder_3(State3 state, Blackhole blackhole) {
        blackhole.consume(new StringBuilder().append(state.a).append(state.b).append(state.c).toString());
    }

    @Benchmark
    @CompilerControl(CompilerControl.Mode.EXCLUDE)
    public void stringbuilder_4(State4 state, Blackhole blackhole) {
        blackhole.consume(new StringBuilder().append(state.a).append(state.b).append(state.c).append(state.d).toString());
    }

    @Benchmark
    @CompilerControl(CompilerControl.Mode.EXCLUDE)
    public void stringbuffer_2(State2 state, Blackhole blackhole) {
        blackhole.consume(new StringBuffer().append(state.a).append(state.b).toString());
    }

    @Benchmark
    @CompilerControl(CompilerControl.Mode.EXCLUDE)
    public void stringbuffer_3(State3 state, Blackhole blackhole) {
        blackhole.consume(new StringBuffer().append(state.a).append(state.b).append(state.c).toString());
    }

    @Benchmark
    @CompilerControl(CompilerControl.Mode.EXCLUDE)
    public void stringbuffer_4(State4 state, Blackhole blackhole) {
        blackhole.consume(new StringBuffer().append(state.a).append(state.b).append(state.c).append(state.d).toString());
    }

    @Benchmark
    @CompilerControl(CompilerControl.Mode.EXCLUDE)
    public void concat_2(State2 state, Blackhole blackhole) {
        blackhole.consume(state.a.concat(state.b));
    }

    @Benchmark
    @CompilerControl(CompilerControl.Mode.EXCLUDE)
    public void concat_3(State3 state, Blackhole blackhole) {
        blackhole.consume(state.a.concat(state.b.concat(state.c)));
    }

    @Benchmark
    @CompilerControl(CompilerControl.Mode.EXCLUDE)
    public void concat_4(State4 state, Blackhole blackhole) {
        blackhole.consume(state.a.concat(state.b.concat(state.c.concat(state.d))));
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(StringConcatenationNoJitBench.class.getSimpleName())
                //.warmupIterations(5)
                //.measurementIterations(7)
                //.jvm("C:\\Program Files\\Java\\jre1.8.0_144\\bin\\javaw.exe")
                .jvmArgs("-Xms3g", "-Xmx3g")
                .shouldDoGC(true)
                .threads(4)
                .forks(1)
                .build();

        new Runner(opt).run();
    }
}
