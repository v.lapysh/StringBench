package com.presentation.benchmark.concatenation;

public class StringConcatenation {
    public static final String AAA = "aaa";

    public static void main(String[] args) {
        String b = "b";
        String str = b + "abc" + "abcabc" + "abcabcabc" + AAA;
        System.out.println("str = " + str);

        StringBuilder builder = new StringBuilder();
        builder.append("abcabcabc")
                .append("abcabc")
                .append("abc")
                .append(AAA)
                .append(b);
        System.out.println("builder.toString() = " + builder.toString());
    }
}
